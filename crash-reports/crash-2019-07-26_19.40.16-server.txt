---- Minecraft Crash Report ----
// Don't be sad, have a hug! <3

Time: 7/26/19 7:40 PM
Description: Exception in server tick loop

com.google.gson.JsonSyntaxException: com.google.gson.stream.MalformedJsonException: Expected ':' at line 4 column 2 path $[0].lordcrts
	at com.google.gson.internal.Streams.parse(Streams.java:60)
	at com.google.gson.internal.bind.TreeTypeAdapter.read(TreeTypeAdapter.java:65)
	at com.google.gson.internal.bind.TypeAdapterRuntimeTypeWrapper.read(TypeAdapterRuntimeTypeWrapper.java:41)
	at com.google.gson.internal.bind.CollectionTypeAdapterFactory$Adapter.read(CollectionTypeAdapterFactory.java:82)
	at com.google.gson.internal.bind.CollectionTypeAdapterFactory$Adapter.read(CollectionTypeAdapterFactory.java:61)
	at net.minecraft.util.JsonUtils.func_193838_a(SourceFile:504)
	at net.minecraft.util.JsonUtils.func_193841_a(SourceFile:522)
	at net.minecraft.server.management.UserList.func_152679_g(SourceFile:141)
	at net.minecraft.server.dedicated.DedicatedPlayerList.func_187246_z(SourceFile:99)
	at net.minecraft.server.dedicated.DedicatedPlayerList.<init>(SourceFile:25)
	at net.minecraft.server.dedicated.DedicatedServer.func_71197_b(DedicatedServer.java:220)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:486)
	at java.lang.Thread.run(Unknown Source)
Caused by: com.google.gson.stream.MalformedJsonException: Expected ':' at line 4 column 2 path $[0].lordcrts
	at com.google.gson.stream.JsonReader.syntaxError(JsonReader.java:1559)
	at com.google.gson.stream.JsonReader.doPeek(JsonReader.java:530)
	at com.google.gson.stream.JsonReader.peek(JsonReader.java:425)
	at com.google.gson.internal.bind.TypeAdapters$29.read(TypeAdapters.java:716)
	at com.google.gson.internal.bind.TypeAdapters$29.read(TypeAdapters.java:739)
	at com.google.gson.internal.bind.TypeAdapters$29.read(TypeAdapters.java:714)
	at com.google.gson.internal.Streams.parse(Streams.java:48)
	... 12 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_221, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 4760314648 bytes (4539 MB) / 5048893440 bytes (4815 MB) up to 5048893440 bytes (4815 MB)
	JVM Flags: 2 total; -Xmx5024M -Xms5024M
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.5.2768 9 mods loaded, 9 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State  | ID                | Version       | Source                                        | Signature                                |
	|:------ |:----------------- |:------------- |:--------------------------------------------- |:---------------------------------------- |
	| UCHIJA | minecraft         | 1.12.2        | minecraft.jar                                 | None                                     |
	| UCHIJA | mcp               | 9.42          | minecraft.jar                                 | None                                     |
	| UCHIJA | FML               | 8.0.99.99     | forge-1.12.2-14.23.5.2768-universal.jar       | e3c3d50c7c986df74c645c0ac54639741c90a557 |
	| UCHIJA | forge             | 14.23.5.2768  | forge-1.12.2-14.23.5.2768-universal.jar       | e3c3d50c7c986df74c645c0ac54639741c90a557 |
	| UCHIJA | biomesoplenty     | 7.0.1.2438    | BiomesOPlenty-1.12.2-7.0.1.2438-universal.jar | None                                     |
	| UCHIJA | customspawner     | 3.11.4        | CustomMobSpawner-3.11.4.jar                   | None                                     |
	| UCHIJA | mocreatures       | 12.0.5        | DrZharks MoCreatures Mod-12.0.5.jar           | None                                     |
	| UCHIJA | ic2               | 2.8.170-ex112 | industrialcraft-2-2.8.170-ex112.jar           | de041f9f6187debbc77034a344134053277aa3b0 |
	| UCHIJA | mercurius_updater | 1.0           | MercuriusUpdater-1.12.2.jar                   | None                                     |

	Loaded coremods (and transformers): 
	Profiler Position: N/A (disabled)
	Is Modded: Definitely; Server brand changed to 'fml,forge'
	Type: Dedicated Server (map_server.txt)